import org.lwjgl.*;
import org.lwjgl.glfw.*;
import org.lwjgl.opengl.*;
import org.lwjgl.system.*;

import java.nio.*;

import static org.lwjgl.glfw.Callbacks.*;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.system.MemoryStack.*;
import static org.lwjgl.system.MemoryUtil.*;
import java.util.concurrent.TimeUnit;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.*;
import org.opencv.videoio.*;
public class Main {

	// The window handle
	private static long window;
	int framesProcessed = 0;

	public void run() {
        System.out.println("LWJGL " + Version.getVersion());

        //OpenCV
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Globals.capture = new VideoCapture("src/video.mkv");
        if(Globals.capture.isOpened())
        {
            System.out.println("OpenCV loaded.");
        }
        else
        {
            System.exit(0);
        }
		ImageBuffer t1 = new ImageBuffer("test");
		t1.start();
		init();
		var start = System.currentTimeMillis();
		loop();
		var end = System.currentTimeMillis();
		var elapsed_MS = end - start;
		var fps = framesProcessed / (elapsed_MS/1000);

		System.out.println("MS: " + elapsed_MS + " FPS: " + fps);

		// Free the window callbacks and destroy the window
		glfwFreeCallbacks(window);
		glfwDestroyWindow(window);

		// Terminate GLFW and free the error callback
		glfwTerminate();
		glfwSetErrorCallback(null).free();

		try {
			t1.join();
		} catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}

	private static GLFWFramebufferSizeCallback resizeWindowCallback = new GLFWFramebufferSizeCallback(){
		@Override
		public void invoke(long window, int width, int height){
			System.out.println("Width: " + width +  " Height: " + height);
			  glViewport(0,0,width,height);
			  Globals.window_height = height;
			  Globals.window_width = width;
		  	//update any other window vars you might have (aspect ratio, MVP matrices, etc)
		}
	  };

	private void init() {
		// Setup an error callback. The default implementation
		// will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. Most GLFW functions will not work before doing this.
		if ( !glfwInit() )
			throw new IllegalStateException("Unable to initialize GLFW");

		// Configure GLFW
		glfwDefaultWindowHints(); // optional, the current window hints are already the default
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE); // the window will be resizable

		// Create the window
		window = glfwCreateWindow(Globals.window_width, Globals.window_height, "Barthilas - Tracker", NULL, NULL);
		if ( window == NULL )
			throw new RuntimeException("Failed to create the GLFW window");

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
			if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
				glfwSetWindowShouldClose(window, true); // We will detect this in the rendering loop
		});
			

		// Get the thread stack and push a new frame
		try ( MemoryStack stack = stackPush() ) {
			IntBuffer pWidth = stack.mallocInt(1); // int*
			IntBuffer pHeight = stack.mallocInt(1); // int*

			// Get the window size passed to glfwCreateWindow
			glfwGetWindowSize(window, pWidth, pHeight);

			// Get the resolution of the primary monitor
			GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());

			// Center the window
			glfwSetWindowPos(
				window,
				(vidmode.width() - pWidth.get(0)) / 2,
				(vidmode.height() - pHeight.get(0)) / 2
			);
		} // the stack frame is popped automatically

		// Make the OpenGL context current
		glfwMakeContextCurrent(window);
		// Enable v-sync
		glfwSwapInterval(1);

		// Make the window visible
		glfwShowWindow(window);
	}

	private void loop() {
		// This line is critical for LWJGL's interoperation with GLFW's
		// OpenGL context, or any context that is managed externally.
		// LWJGL detects the context that is current in the current thread,
		// creates the GLCapabilities instance and makes the OpenGL
		// bindings available for use.
		GL.createCapabilities();
		GLFW.glfwSetFramebufferSizeCallback(window, resizeWindowCallback);

		Mat drawImg = new Mat();
		Point p = new Point();

		// Set the clear color - RED
		glClearColor(1.0f, 0.0f, 0.0f, 0.0f);

		// Run the rendering loop until the user has attempted to close
		// the window or has pressed the ESCAPE key.
		while ( !glfwWindowShouldClose(window) ) {
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer
            // Use ModelView matrix for following trasformations (translate,rotate,scale)
            glMatrixMode(GL_MODELVIEW);
            // Clear all tranformations
			glLoadIdentity();
			// Draw something
			GL11.glOrtho(0, Globals.window_width, 0, Globals.window_height, 1, -1);
			glRasterPos2i(0, 0);
			try {
				p = Globals.blockingQueue.poll(100, TimeUnit.MILLISECONDS);
				System.out.println(p);
				drawImg = Globals.blockingQueueFrame.poll(100, TimeUnit.MILLISECONDS);
				//System.out.println(drawImg);
			} catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			if(drawImg == null)
			{
				glfwWindowShouldClose(window);
				break;
			}
			else
			{
				glDrawPixels(drawImg.cols(), drawImg.rows(), GL12.GL_BGR, GL_UNSIGNED_BYTE, drawImg.dataAddr());
				drawImg.empty();
				framesProcessed++;
			}

			glfwSwapBuffers(window); // swap the color buffers

			// Poll for window events. The key callback above will only be
			// invoked during this call.
			glfwPollEvents();
		}
	}

	public static void main(String[] args) {
		new Main().run();
	}

}