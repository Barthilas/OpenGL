
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.videoio.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Globals {
   static VideoCapture capture;
   static int window_width = 600;
   static int window_height = 600;
   static BlockingQueue<Point> blockingQueue = new LinkedBlockingQueue<>();
   static BlockingQueue<Mat> blockingQueueFrame = new LinkedBlockingQueue<>();
   //ConcurrentLinkedQueue queue = new ConcurrentLinkedQueue();
 }