
import java.util.concurrent.TimeUnit;

import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;

class ImageBuffer extends Thread {
    private Thread t;
    private String threadName;
    private FrameProcessor f;
    
    ImageBuffer(String name) {
       threadName = name;
       f = new FrameProcessor();
    }
    
    public void run() {

        boolean grab_flag = true;
        Point centerPoint;

        while (true)
        {
            Mat grabImg = new Mat();
            grab_flag = Globals.capture.read(grabImg);
            //flip -> OpenGL different coords system
            if(grab_flag)
            {
                Core.flip(grabImg, grabImg, -1);
                centerPoint = f.ThresholdImage(grabImg);
                FrameProcessor.drawCross(grabImg, centerPoint.x, centerPoint.y, 5);
                try {
                    Globals.blockingQueueFrame.offer(grabImg, 500, TimeUnit.MILLISECONDS);
                    Globals.blockingQueue.offer(centerPoint, 500, TimeUnit.MILLISECONDS);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            else
            {
                System.out.println("IMAGE BUFFER IS DONE.");
                break;
            }
        }
    }
    public void start () {
       if (t == null) {
          t = new Thread (this, threadName);
          t.start ();
       }
    }
 }