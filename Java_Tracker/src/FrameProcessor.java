
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

public class FrameProcessor {
    Point ThresholdImage(Mat frame)
    {
        double h_low = 128.0;
        double s_low = 128.0;
        double v_low = 128.0;
        double h_hi = 255.0;
        double s_hi = 255.0;
        double v_hi = 255.0;
        Point result = new Point(0, 0);
        int cnt = 0, sumX = 0, sumY = 0;

        // analyze the image

        Mat scene_hsv = new Mat();
        Mat scene_threshold = new Mat();  
        Mat whitePixels = new Mat();

        Imgproc.cvtColor(frame, scene_hsv, Imgproc.COLOR_BGR2HSV);

        Scalar lower_threshold = new Scalar(h_low, s_low, v_low);
        Scalar upper_threshold = new Scalar(h_hi, s_hi, v_hi);
        Core.inRange(scene_hsv, lower_threshold, upper_threshold, scene_threshold);

        Core.findNonZero(scene_threshold, whitePixels);
        cnt = (int)whitePixels.size().height;
        for (int i = 0; i < cnt; i++)
	    {
            var x_y_coord = whitePixels.get(i, 0);
            Point point = new Point(x_y_coord[0], x_y_coord[1]);

            sumX += point.x;
            sumY += point.y;

        }

        //my_mutex.lock();
        if (cnt > 0)
        {
            result.x = sumX / cnt;
            result.y = sumY / cnt;
        }
        else
        {
            result.x = 0;
            result.y = 0;
        }
        //my_mutex.unlock();
        
        return result;
    }
    static void drawCross(Mat img, double x, double y, int size) {
        Point p1 = new Point(x - size / 2, y);
        Point p2 = new Point(x + size / 2, y);
        Point p3 = new Point(x, y - size / 2);
        Point p4 = new Point(x, y + size / 2);
    
        Imgproc.line(img, p1, p2, new Scalar(0,255,0), 2, 1);
        Imgproc.line(img, p3, p4, new Scalar(0,255,0), 2, 1);
    }
}
