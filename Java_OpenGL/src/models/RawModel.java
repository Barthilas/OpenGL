package src.models;
/**
 * VAO (Vertex Array Object) 
 * - store data about 3D model to ATTRIBUTES
 * - up to 15 indexes
 * - unique ID for each VAO
 * - vertex pos, vertex color, normals, textures...
 * VBO (Vertex Buffered Object)
 * - stored in VAO on index
 * - data, array of numbers
 */
public class RawModel {
    private int vaoID;
    private int vertexCount;

    public RawModel(int vaoID, int vertexCount) {
        this.setVaoID(vaoID);
        this.setVertexCount(vertexCount);
    }

    public int getVaoID() {
        return vaoID;
    }

    public void setVaoID(int vaoID) {
        this.vaoID = vaoID;
    }

    public int getVertexCount() {
        return vertexCount;
    }

    public void setVertexCount(int vertexCount) {
        this.vertexCount = vertexCount;
    }
}
