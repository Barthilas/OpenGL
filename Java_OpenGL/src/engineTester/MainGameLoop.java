package src.engineTester;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.Texture;
import org.w3c.dom.Text;

import src.renderEngine.DisplayManager;
import src.renderEngine.Loader;
import src.renderEngine.MasterRenderer;
import src.renderEngine.OBJLoader;
import src.models.RawModel;
import src.models.TexturedModel;
import src.particles.Particle;
import src.particles.ParticleMaster;
import src.particles.ParticleSystem;
import src.particles.ParticleTexture;
import src.terrains.Terrain;
import src.textures.ModelTexture;
import src.textures.TerrainTexture;
import src.textures.TerrainTexturePack;
import src.entities.Camera;
import src.entities.Entity;
import src.entities.Light;
import src.entities.Player;
import src.guis.GuiRenderer;
import src.guis.GuiTexture;


public class MainGameLoop {
    public static void main(String[] args) {
		System.out.println("OS name " + System.getProperty("os.name"));
    	System.out.println("OS version " + System.getProperty("os.version"));
		System.out.println("LWJGL version " + org.lwjgl.Sys.getVersion());
        DisplayManager.createDisplay();
		Loader loader = new Loader();		
		List<Light> lights = new ArrayList<Light>();
		MasterRenderer renderer = new MasterRenderer(loader);
		ParticleMaster.init(loader, renderer.getProjectionMatrix());
		//lights.add(new Light(new Vector3f(0, 1000, -7000), new Vector3f(0.4f, 0.4f, 0.4f)));

		TerrainTexture backgroundTexture = new TerrainTexture(loader.loadTexture("grassy"));
		TerrainTexture rTexture = new TerrainTexture(loader.loadTexture("dirt"));
		TerrainTexture gTexture = new TerrainTexture(loader.loadTexture("pinkFlowers"));
		TerrainTexture bTexture = new TerrainTexture(loader.loadTexture("path"));
		TerrainTexture blendMap = new TerrainTexture(loader.loadTexture("blendMap"));

		TerrainTexturePack texturePack = new TerrainTexturePack(backgroundTexture, rTexture, gTexture, bTexture);

		Terrain terrain = new Terrain(-1,-1,loader, texturePack, blendMap, "heightmap");
		Terrain terrain2 = new Terrain(0,-1,loader, texturePack, blendMap, "heightmap");
		List<Terrain> terrains = new ArrayList<Terrain>();
		terrains.add(terrain);
		terrains.add(terrain2);

		TexturedModel tree = new TexturedModel(OBJLoader.loadObjModel("tree", loader), new ModelTexture(loader.loadTexture("tree")));
		TexturedModel pine = new TexturedModel(OBJLoader.loadObjModel("pine", loader), new ModelTexture(loader.loadTexture("pine")));
        TexturedModel grass = new TexturedModel(OBJLoader.loadObjModel("grassModel", loader), new ModelTexture(loader.loadTexture("grassTexture")));
        grass.getTexture().setHasTransparency(true);
		grass.getTexture().setUseFakeLightning(true);
		tree.getTexture().setReflectivity(0.1f);
		tree.getTexture().setShineDamper(0.5f);
		
		TexturedModel fern = new TexturedModel(OBJLoader.loadObjModel("fern", loader), new ModelTexture(loader.loadTexture("fern")));
        fern.getTexture().setHasTransparency(true);
		fern.getTexture().setUseFakeLightning(true);
		fern.getTexture().setNumberOfRows(2);

		TexturedModel tree1 = new TexturedModel(OBJLoader.loadObjModel("lowPolyTree", loader), new ModelTexture(loader.loadTexture("lowPolyTree")));
		TexturedModel lamp = new TexturedModel(OBJLoader.loadObjModel("lamp", loader), new ModelTexture(loader.loadTexture("lamp")));
		lamp.getTexture().setUseFakeLightning(true);


		List<Entity> entities = new ArrayList<Entity>();
		List<Entity> entities2 = new ArrayList<Entity>();
		HashMap<Terrain, List<Entity>> map = new HashMap<>();
		map.put(terrain, entities);
		map.put(terrain2, entities2);
		var random = new Random(56487441);
		for(int i=0;i<500;i++){
			for (Terrain terr : terrains) {
					float x = random.nextFloat() * 800 - 400;
					float z = random.nextFloat() * -600;
					float y = terr.getHeightOfTerrain(x, z);
					if(terr.areCoordsValid(x, z))
					{
						map.get(terr).add(new Entity(fern,random.nextInt(4), new Vector3f(x,y,z),0,0,0,0.6f));
					}
					/*
					x = random.nextFloat() * 800 - 400;
					z = random.nextFloat() * -600;
					y = terr.getHeightOfTerrain(x, z);
					if(terr.areCoordsValid(x, z, terr))
					{
						map.get(terr).add(new Entity(tree, new Vector3f(x,y,z),0,0,0,3));
					}

					x = random.nextFloat() * 800 - 400;
					z = random.nextFloat() * -600;
					y = terr.getHeightOfTerrain(x, z);
					if(terr.areCoordsValid(x, z, terr))
					{
						map.get(terr).add(new Entity(tree1, new Vector3f(x,y,z),0,0,0,0.4f));
					}
					*/
					x = random.nextFloat() * 800 - 400;
					z = random.nextFloat() * -600;
					y = terr.getHeightOfTerrain(x, z);
					if(terr.areCoordsValid(x, z))
					{
						map.get(terr).add(new Entity(pine, new Vector3f(x,y,z),0,0,0,1));
					}
				}

		}
		//create 4 lights (2 lights, 2 terrains) => for more edit shader
		float lightOffset = 15;
		/*
		for(int i=0;i<2;i++)
		{
			for(Terrain terr : terrains)
			{
				float x = random.nextFloat() * 800 - 400;
				float z = random.nextFloat() * -600;
				float y = terr.getHeightOfTerrain(x, z);
				map.get(terr).add(new Entity(lamp, new Vector3f(x,y, z), 0, 0, 0, 1));
				lights.add(new Light(new Vector3f(x, y+lightOffset, z), new Vector3f(0.5f,0.4f,0.0f), new Vector3f(1,0.001f,0.002f)));
				
			}
		}d
		*/
		
		entities.add(new Entity(lamp, new Vector3f(10,terrain.getHeightOfTerrain(10, -50), -50), 0, 0, 0, 1));
		lights.add(new Light(new Vector3f(10, terrain.getHeightOfTerrain(10, -50)+lightOffset, -50), new Vector3f(0.5f,0.4f,0.0f), new Vector3f(0.3f,0.001f,0.002f)));

		entities.add(new Entity(lamp, new Vector3f(150,terrain2.getHeightOfTerrain(150, -100), -100), 0, 0, 0, 1));
		lights.add(new Light(new Vector3f(150, terrain2.getHeightOfTerrain(150, -100)+lightOffset, -100), new Vector3f(0.5f,0.0f,0.0f), new Vector3f(1,0.001f,0.002f)));

		RawModel golemModel = OBJLoader.loadObjModel("person", loader);
		TexturedModel golemTextured = new TexturedModel(golemModel, new ModelTexture(loader.loadTexture("playerTexture")));
		Player player = new Player(golemTextured, new Vector3f(100, 0, -50), 0, 0, 0, 0.5f);
		Camera camera = new Camera(player);

		GuiRenderer guiRenderer = new GuiRenderer(loader);
		List<GuiTexture> guis = new ArrayList<GuiTexture>();
		GuiTexture gui = new GuiTexture(loader.loadTexture("health"), new Vector2f(-0.75f, -0.85f), new Vector2f(0.2f, 0.1f));
		guis.add(gui);

		ParticleTexture particleTexture = new ParticleTexture(loader.loadTexture("particleAtlas"), 4, true);
		ParticleSystem system = new ParticleSystem(particleTexture,50, 25, 0.3f, 4);

		while(!Display.isCloseRequested()){

			for (Terrain t2 : terrains) {
				if (player.getPosition().x < t2.getX() + t2.getSize() && player.getPosition().x >= t2.getX() &&
				 player.getPosition().z >= t2.getZ() && player.getPosition().z < t2.getZ() + t2.getSize()) {
					  player.move(t2);
					  //player stuck
					  if(!t2.areCoordsValid(player.getPosition().x, player.getPosition().z))
					  {
						Vector3f resetLoc = player.getPosition();
						resetLoc.z =- 1;
						player.setPosition(resetLoc);
					  }
				  }
				}
			
			camera.move();
			if(Keyboard.isKeyDown(Keyboard.KEY_SPACE))
			{
				//new Particle(new Vector3f(player.getPosition()), new Vector3f(0,30,0), 1,4,0,1);
				system.generateParticles(player.getPosition());
			}
			ParticleMaster.update(camera);
			//player.move(terrain2);
			renderer.processEntity(player);
			renderer.processTerrain(terrain);
			renderer.processTerrain(terrain2);
			for(Terrain ter:terrains)
			{
				List<Entity> terrainEntities =  map.get(ter);
				for(Entity entity:terrainEntities)
				{
					renderer.processEntity(entity);
				}
			}
			renderer.render(lights, camera);
			ParticleMaster.renderParticles(camera);
			guiRenderer.render(guis);
			DisplayManager.updateDisplay();
		}

		ParticleMaster.cleanUp();
		guiRenderer.cleanUp();
		renderer.cleanUp();
		loader.cleanUp();
		DisplayManager.closeDisplay();
        
	}
}
