package src.entities;

import org.lwjgl.util.vector.Vector3f;

import src.models.TexturedModel;

public class Entity {
    private TexturedModel model;
    private Vector3f position;
    private float rotX, rotY, rotZ;
    private float scale;

    private int textureIndex = 0; //top to right and bottom

    public Entity(TexturedModel model, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        this.setModel(model);
        this.setPosition(position);
        this.setRotX(rotX);
        this.setRotY(rotY);
        this.setRotZ(rotZ);
        this.setScale(scale);
    }
    public Entity(TexturedModel model, int textureIndex, Vector3f position, float rotX, float rotY, float rotZ, float scale) {
        this.setModel(model);
        this.setPosition(position);
        this.setRotX(rotX);
        this.setRotY(rotY);
        this.setRotZ(rotZ);
        this.setScale(scale);
        this.textureIndex = textureIndex;
    }
    public float getTextureXOffset()
    {
        int numberofColumns = model.getTexture().getNumberOfRows();
        int column = textureIndex%numberofColumns;
        return (float)column/(float)numberofColumns;
    }
    public float getTextureYOffset()
    {
        int numberofRows = model.getTexture().getNumberOfRows();
        int row = textureIndex/numberofRows;
        return (float)row/(float)numberofRows;
    }

    public void increasePosition(float dx, float dy, float dz)
    {
        this.position.x +=dx;
        this.position.y +=dy;
        this.position.z +=dz;
    }

    public void increaserotation(float dx, float dy, float dz)
    {
        this.rotX+=dx;
        this.rotY+=dy;
        this.rotZ+=dz;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public float getRotZ() {
        return rotZ;
    }

    public void setRotZ(float rotZ) {
        this.rotZ = rotZ;
    }

    public float getRotY() {
        return rotY;
    }

    public void setRotY(float rotY) {
        this.rotY = rotY;
    }

    public float getRotX() {
        return rotX;
    }

    public void setRotX(float rotX) {
        this.rotX = rotX;
    }

    public Vector3f getPosition() {
        return position;
    }

    public void setPosition(Vector3f position) {
        this.position = position;
    }

    public TexturedModel getModel() {
        return model;
    }

    public void setModel(TexturedModel model) {
        this.model = model;
    }
}
